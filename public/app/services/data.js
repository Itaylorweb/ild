'use strict';


app.factory('dataFactory', ['$http', function($http) {

	var data;

	return {
		get: function() {
			return data || [];
		},
		set: function(d) {
			data = d;
		}
	};


}]);