'use strict';

var app = angular.module('iLetDirect', ['ngMaterial', 'ui.router']);

app.config(function($locationProvider, $urlRouterProvider, $mdThemingProvider, $mdIconProvider) {

	$locationProvider.html5Mode(true);

	$urlRouterProvider.otherwise('/');
  
    var iLetDirectMap = $mdThemingProvider.extendPalette('blue', {
        '700': '#1396e2'
    });
    
    $mdThemingProvider.definePalette('iLetDirect', iLetDirectMap);

    $mdThemingProvider.theme('default')
    .primaryPalette('iLetDirect');

    // var customPrimary = {
    //     '50': '#afddf8',
    //     '100': '#98d4f6',
    //     '200': '#80caf4',
    //     '300': '#69c0f2',
    //     '400': '#51b6f0',
    //     '500': '#3aacee',
    //     '600': '#22a2ec',
    //     '700': '#1396e2',
    //     '800': '#1186ca',
    //     '900': '#0f77b3',
    //     'A100': '#c7e7fa',
    //     'A200': '#def1fc',
    //     'A400': '#f6fbfe',
    //     'A700': '#0d679b'
    // };
    // $mdThemingProvider
    //     .definePalette('customPrimary', 
    //                     customPrimary);
    var customBackground = {
        '50': '#ffffff',
        '100': '#ffffff',
        '200': '#ffffff',
        '300': '#ffffff',
        '400': '#ffffff',
        '500': '#FFF',
        '600': '#f2f2f2',
        '700': '#e6e6e6',
        '800': '#d9d9d9',
        '900': '#cccccc',
        'A100': '#ffffff',
        'A200': '#ffffff',
        'A400': '#ffffff',
        'A700': '#bfbfbf'
    };
    $mdThemingProvider
        .definePalette('customBackground', 
                        customBackground);

    $mdThemingProvider.theme('whiteToolbar')
    .primaryPalette('customBackground')
    .accentPalette('blue');

    $mdThemingProvider.theme('default')
    .primaryPalette('blue')
    .accentPalette('blue-grey')
    .backgroundPalette('customBackground');

    $mdIconProvider
    // linking to https://github.com/google/material-design-icons/tree/master/sprites/svg-sprite
    // 
    .iconSet('action', '/assets/stylesheets/svg-sprite-action.svg', 24)
    .iconSet('alert', '/assets/stylesheets/svg-sprite-alert.svg', 24)
    .iconSet('av', '/assets/stylesheets/svg-sprite-av.svg', 24)
    .iconSet('communication', '/assets/stylesheets/svg-sprite-communication.svg', 24)
    .iconSet('content', '/assets/stylesheets/svg-sprite-content.svg', 24)
    .iconSet('device', '/assets/stylesheets/svg-sprite-device.svg', 24)
    .iconSet('editor', '/assets/stylesheets/svg-sprite-editor.svg', 24)
    .iconSet('file', '/assets/stylesheets/svg-sprite-file.svg', 24)
    .iconSet('hardware', '/assets/stylesheets/svg-sprite-hardware.svg', 24)
    .iconSet('image', '/assets/stylesheets/svg-sprite-image.svg', 24)
    .iconSet('maps', '/assets/stylesheets/svg-sprite-maps.svg', 24)
    .iconSet('navigation', '/assets/stylesheets/svg-sprite-navigation.svg', 24)
    .iconSet('notification', '/assets/stylesheets/svg-sprite-notification.svg', 24)
    .iconSet('social', '/assets/stylesheets/svg-sprite-social.svg', 24)
    .iconSet('toggle', '/assets/stylesheets/svg-sprite-toggle.svg', 24)
    
    // Illustrated user icons used in the docs https://material.angularjs.org/latest/#/demo/material.components.gridList
    .iconSet('avatars', '/assets/stylesheets/avatar-icons.svg', 24)
    .defaultIconSet('/assets/stylesheets/svg-sprite-action.svg', 24);

})
.factory('httpInterceptor', ['$q','$location',function ($q,$location) {
    return {
        'response': function(response) {
            if (response.status === 401) {
            	console.log('Access Denied');
                return $q.reject(response);
            }
            return response || $q.when(response);
        },
        'responseError': function(rejection) {

            if (rejection.status === 401) {
            	console.log('Access Denied');
                return $q.reject(rejection);
            }
            return $q.reject(rejection);
        }
    };
}])
//http Intercpetor to check auth failures for xhr requests
.config(['$httpProvider',function($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
}])
.filter('capitalize', function() {
    return function(input, all) {
      return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
  };
})
.run(function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.isLoadingView = false;

    $rootScope.$on('$stateChangeStart', function(evt, toState) {
        $rootScope.isLoadingView = true;
    });
    $rootScope.$on('$stateChangeSuccess', function (evt, toState, toParams, fromState, fromParams) {
        $rootScope.isLoadingView = false;
    });
});