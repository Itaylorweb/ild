/* jslint node: true */
'use strict';

app.config(function ($stateProvider) {

	$stateProvider
	.state('home', {
		url: '/',
		templateUrl: '/app/partials/home.html',
		controller: 'homeCtrl'
	});

});