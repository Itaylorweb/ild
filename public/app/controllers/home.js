'use strict';

app.controller('homeCtrl', ['$scope', '$mdMedia', function($scope, $mdMedia) {
	// Home angular entry point

	$scope.mdMedia = $mdMedia;
}]);