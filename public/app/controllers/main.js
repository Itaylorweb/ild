'use strict';

function DialogController($scope, $mdDialog) {
	$scope.hide = function() {
		$mdDialog.hide();
	};
	$scope.cancel = function() {
		$mdDialog.cancel();
	};
	$scope.answer = function(answer) {
		$mdDialog.hide(answer);
	};
}


app.controller('mainCtrl', ['$scope', '$mdBottomSheet','$mdSidenav', '$mdDialog', function($scope, $mdBottomSheet, $mdSidenav, $mdDialog){

  // Toolbar search toggle
  $scope.toggleSearch = function(element) {
  	$scope.showSearch = !$scope.showSearch;
  };
  
  // Sidenav toggle
  $scope.toggleSidenav = function(menuId) {
  	$mdSidenav(menuId).toggle();
  };
  
  // Menu items
  $scope.menu = [
  {
  	link : 'home',
  	title: 'Dashboard',
    icon: 'action:ic_dashboard_24px' // we have to use Google's naming convention for the IDs of the SVGs in the spritesheet
  },
  {
  	link : 'home',
  	title: 'Reports',
  	icon: 'action:ic_assessment_24px'
  },
  {
  	link : 'thepolicyshop',
  	title: 'Upload',
  	icon: 'file:ic_file_upload_24px'
  }
  ];
  $scope.admin = [
  {
  	link : '',
  	title: 'Providers',
  	icon: 'communication:ic_business_24px'
  },
  {
  	link : 'showListBottomSheet($event)',
  	title: 'Settings',
  	icon: 'action:ic_settings_24px'
  }
  ];
  
  $scope.providers = [
    {
      name: 'Bedfords',
      link: 'bedfords',
      image: '/assets/images/Bedfordinsurance.png'
    },
    {
      name: 'Van Compare',
      link: 'vancompare',
      image: '/assets/images/VanCompare.png'
    },
    {
      name: 'The Policy Shop',
      link: 'thepolicyshop',
      image: '/assets/images/ThePolicyShop.png'
    },
    {
      name: 'Saffron',
      link: 'saffron',
      image: '/assets/images/saffron.gif'
    },
    {
      name: 'SureThing',
      link: 'surething',
      image: '/assets/images/sure-thing-logo.png'
    }
  ];

  // Mock activity
  $scope.activity = [
  {
  	what: 'Brunch this weekend?',
  	who: 'Ali Conners',
  	avatar: 'svg-1',
  	when: '3:08PM',
  	notes: ' I\'ll be in your neighborhood doing errands'
  },
  {
  	what: 'Summer BBQ',
  	who: 'to Alex, Scott, Jennifer',
  	avatar: 'svg-2',
  	when: '3:08PM',
  	notes: 'Wish I could come out but I\'m out of town this weekend'
  },
  {
  	what: 'Oui Oui',
  	who: 'Sandra Adams',
  	avatar: 'svg-3',
  	when: '3:08PM',
  	notes: 'Do you have Paris recommendations? Have you ever been?'
  },
  {
  	what: 'Birthday Gift',
  	who: 'Trevor Hansen',
  	avatar: 'svg-4',
  	when: '3:08PM',
  	notes: 'Have any ideas of what we should get Heidi for her birthday?'
  },
  {
  	what: 'Recipe to try',
  	who: 'Brian Holt',
  	avatar: 'svg-5',
  	when: '3:08PM',
  	notes: 'We should eat this: Grapefruit, Squash, Corn, and Tomatillo tacos'
  },
  ];
  
  // Bottomsheet & Modal Dialogs
  $scope.alert = '';
  $scope.showListBottomSheet = function($event) {
  	$scope.alert = '';
  	$mdBottomSheet.show({
  		templateUrl: '/app/partials/bottom-sheet.html',
  		controller: 'ListBottomSheetCtrl',
  		targetEvent: $event
  	}).then(function(clickedItem) {
  		$scope.alert = clickedItem.name + ' clicked!';
  	});
  };
  
  $scope.showAdd = function(ev) {
  	$mdDialog.show({
  		controller: DialogController,
  		template: '<md-dialog aria-label="Form"> <md-content class="md-padding"> <form name="userForm"> <div layout layout-sm="column"> <md-input-container flex> <label>First Name</label> <input ng-model="user.firstName"> </md-input-container> <md-input-container flex> <label>Last Name</label> <input ng-model="user.lastName"> </md-input-container> </div> <md-input-container flex> <label>Message</label> <textarea ng-model="user.biography" columns="1" md-maxlength="150"></textarea> </md-input-container> </form> </md-content> <div class="md-actions" layout="row"> <span flex></span> <md-button ng-click="answer(\'not useful\')"> Cancel </md-button> <md-button ng-click="answer(\'useful\')" class="md-primary"> Save </md-button> </div></md-dialog>',
  		targetEvent: ev,
  	})
  	.then(function(answer) {
  		$scope.alert = 'You said the information was "' + answer + '".';
  	}, function() {
  		$scope.alert = 'You cancelled the dialog.';
  	});
  };
}]);

app.controller('ListBottomSheetCtrl', function($scope, $mdBottomSheet) {
	$scope.items = [
	{ name: 'Share', icon: 'social:ic_share_24px' },
	{ name: 'Upload', icon: 'file:ic_cloud_upload_24px' },
	{ name: 'Copy', icon: 'content:ic_content_copy_24px' },
	{ name: 'Print this page', icon: 'action:ic_print_24px' },
	];

	$scope.listItemClick = function($index) {
		var clickedItem = $scope.items[$index];
		$mdBottomSheet.hide(clickedItem);
	};
});


function DemoCtrl ($timeout, $q) {
	var self = this;
    // ******************************
    // Internal methods
    // ******************************
    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
     function querySearch (query) {
     	var results = query ? self.states.filter( createFilterFor(query) ) : [];
     	return results;
     }
    /**
     * Build `states` list of key/value pairs
     */
     function loadAll() {
     	var allStates = 'Ali Conners, Alex, Scott, Jennifer, Sandra Adams, Brian Holt, Trevor Hansen';
     	return allStates.split(/, +/g).map( function (state) {
     		return {
     			value: state.toLowerCase(),
     			display: state
     		};
     	});
     }
    /**
     * Create filter function for a query string
     */
     function createFilterFor(query) {
     	var lowercaseQuery = angular.lowercase(query);
     	return function filterFn(state) {
     		return (state.value.indexOf(lowercaseQuery) === 0);
     	};
     }
    // list of `state` value/display objects
    self.states        = loadAll();
    self.selectedItem  = null;
    self.searchText    = null;
    self.querySearch   = querySearch;
  }

  app.controller('DemoCtrl', DemoCtrl);