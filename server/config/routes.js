'use strict';

module.exports = function(app) {

    // Send some basic starting info to the view
	app.get('*', function(req, res) {
	    res.render('index', {
			user: req.user ? {
	            name: req.user.firstname + ' ' + req.user.lastname,
	            _id: req.user._id,
	            email: req.user.email
	       	} : undefined
		});
	});
};