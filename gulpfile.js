'use strict';

var gulp = require('gulp'),
	util = require('gulp-util'),
	sass = require('gulp-sass'),
	cssmin = require('gulp-cssmin'),
	rename = require('gulp-rename'),	
	livereload = require('gulp-livereload'),
	server = require('gulp-develop-server'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify');

// Restart server function - Called on server file(s) changes
function restart( file ) {	
    server.changed( function( error ) {
        if( ! error ) livereload.changed( file.path );
    });
}

var vendorJS = [
],
angularJS = [
	'source/vendor/angular/angular.js',
	'source/vendor/angular-ui-router/release/angular-ui-router.js',
	'source/vendor/angular-animate/angular-animate.js',
	'source/vendor/angular-aria/angular-aria.js',
	'source/vendor/angular-material/angular-material.js'
],
appJS = [
	'public/app/**/*.js'
];

// Custom Styles SASS Bulid
gulp.task('build-css', function() {
	return gulp.src('source/scss/**/*.scss')
	.pipe(sass())
	.pipe(sass().on('error', sass.logError))
	.pipe(cssmin())
    .pipe(rename({suffix: '.min'}))		
	.pipe(gulp.dest('public/assets/stylesheets'))
	.pipe(livereload());
});

// Material SASS Build
gulp.task('build-ma', function() {
	return gulp.src('source/vendor/angular-material/angular-material.scss')
	.pipe(sass())
	.pipe(sass().on('error', sass.logError))
	.pipe(cssmin())
    .pipe(rename({suffix: '.min'}))	
	.pipe(gulp.dest('public/assets/stylesheets'))
	.pipe(livereload());
});

// Vendor JS Build
gulp.task('build-vendorjs', function() {
	return gulp.src(vendorJS)
	.pipe(concat('vendor-bundle.js'))
	.pipe(uglify()) 
	.pipe(gulp.dest('public/assets/javascript'))
	.pipe(livereload());
});

// Angular JS Build
gulp.task('build-angularjs', function() {
	return gulp.src(angularJS)
	.pipe(concat('angular-bundle.js'))
	.pipe(uglify()) 
	.pipe(gulp.dest('public/assets/javascript'))
	.pipe(livereload());
});

// Server start script
gulp.task( 'server:start', function() {
    server.listen({ path: 'server.js' }, livereload.listen() );
});

// Watch Task(s)
gulp.task('watch', function() {	

    // Server Files
	gulp.watch(['server.js', 'server/**/*.js']).on( 'change', restart );

	// Custom Styles
	gulp.watch('source/scss/**/*.scss', ['build-css']);

	// Material
	gulp.watch('source/vendor/angular-material/**/*.scss', ['build-ma']);

	// html
	gulp.watch(['server/views/**/*.html', 'public/app/partials/**/*.html']).on('change', function(file) {
		livereload.changed( file.path );
	});

	// App JS
	gulp.watch(appJS).on('change', function(file) {
		livereload.changed( file.path );
	});

});

// Default task.  Runs server start script, performs a full build then watches for changes
gulp.task('default', ['server:start', 'build-css', 'build-ma', 'build-vendorjs', 'build-angularjs', 'watch']);

