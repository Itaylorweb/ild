'use strict';

var http = require('http'),
express = require('express'),
bodyParser = require('body-parser'),
app = express();

app.engine('html', require('ejs').renderFile);
app.set('views', __dirname + '/server/views');
app.set('view engine', 'html');
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

// routes ==================================================
require('./server/config/routes')(app); // configure our routes

http.createServer(app).listen(3030);